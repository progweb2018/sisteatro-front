import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

import { User } from './models/user';

export const TOKEN_NAME = 'jwt_token';
export const USER_INFO = 'user_info';

import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { URL_API } from '../app.config';

@Injectable()
export class AuthService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  getAccessToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setAccessToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  getUserInfo() {
    return JSON.parse(localStorage.getItem(USER_INFO));
  }

  login(user: User) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    const username = user.username;
    const password = user.password;

    console.log('vou tentar logar', user);
    return this.http
      .post<any>(`${URL_API}/auth`, {username, password}, httpOptions)
        .pipe(map(response => {
          // salva o token na localStorage
          let token = response['token'];
          token = token.replace(`"`, '');
          localStorage.setItem(TOKEN_NAME, JSON.stringify(token));

        return response;
      }));
  }

  logout() {
    console.log('logout');
    this.router.navigate(['login']);
    localStorage.removeItem(TOKEN_NAME);
  }

}
