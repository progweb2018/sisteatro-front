import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alterar-senha',
  templateUrl: './alterar-senha.component.html'
})
export class AlterarSenhaComponent implements OnInit {

  alterarSenhaForm: FormGroup;
  // token: Token;

  constructor(
    private builder: FormBuilder,
    // private router: Router,
  ) { }

  ngOnInit() {
     /* Reactive Forms */
     this.alterarSenhaForm = this.builder.group({
      senhaAtual: this.builder.control('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
      senhaNova: this.builder.control('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
      confirmarSenha: this.builder.control('', [Validators.required, Validators.minLength(6), Validators.maxLength(16)]),
    });
  }

  alterarSenha (senha: any) {
    console.log('Senha alterada!! ' + senha);
  }
}
