import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AlterarSenhaComponent } from './alterar-senha.component';
import { AlterarSenhaRoutingModule } from './alterar-senha-routing.module';

@NgModule({
  imports: [
    AlterarSenhaRoutingModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AlterarSenhaComponent,
  ],
  providers: [
  ]
})
export class AlterarSenhaModule {
}
