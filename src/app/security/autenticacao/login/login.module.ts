import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { AuthService } from '../../auth.service';

@NgModule({
  imports: [
    LoginRoutingModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    LoginComponent,
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    AuthService,
  ]
})
export class LoginModule {
}
