import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';

import { Token } from '../../models/token';
import { AuthService } from '../../auth.service';
import { User } from '../../models/user';
// import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  recuperarSenhaForm: FormGroup;
  user: User = new User();
  token: Token;
  // usuarioDTO: UsuarioDTO;
  recuperarSenha = false;

  constructor(
    private builder: FormBuilder,
    private router: Router,
    public authService: AuthService,
    // public toastr: ToastsManager,
    // vcr: ViewContainerRef
  ) {
    // this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {

    /* Reactive Forms */
    this.loginForm = this.builder.group({
      username: this.builder.control('', [Validators.required]),
      password: this.builder.control('', [Validators.required]),
    }, {});

  }

  login(user: User) {
    if (user) {
      this.authService.login(user)
      .subscribe( (token: Token) => {
        this.token = token;
        // this.usuarioDTO = token.usuarioDTO;
        this.router.navigate(['/home']);
        // this.toastr.success('Login realizado com sucesso!', 'Sucesso!');
        // this.appInterceptor.showSuccess('mensagem.sucesso.login', '/dashboard')
      });
    }
  }

}
