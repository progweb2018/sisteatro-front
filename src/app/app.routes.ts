import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import { PagPrincipalComponent } from './_layout/pag-principal/pag-principal.component';
import { PagNaoEncontradaComponent } from './_layout/pag-nao-encontrada/pag-nao-encontrada.component';
import { AuthGuard } from './_helpers/auth.guard';


const appRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full'},
    { path: 'home', component: PagPrincipalComponent, canActivate: [AuthGuard]},
    { path: 'sala', loadChildren: './dominio/sala/sala.module#SalaModule', canActivate: [AuthGuard]},
    { path: 'espetaculo', loadChildren: './dominio/espetaculo/espetaculo.module#EspetaculoModule', canActivate: [AuthGuard]},
    { path: 'sessao', loadChildren: './dominio/sessao/sessao.module#SessaoModule', canActivate: [AuthGuard]},
    { path: 'funcionario', loadChildren: './dominio/funcionario/funcionario.module#FuncionarioModule', canActivate: [AuthGuard]},
    { path: 'login', loadChildren: './security/autenticacao/login/login.module#LoginModule'},

    { path: '**', component: PagNaoEncontradaComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(
        appRoutes,
        { enableTracing: true }
    )],
    exports: [RouterModule]
  })

  export class AppRoutes {}
