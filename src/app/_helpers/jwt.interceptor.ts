import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TOKEN_NAME } from '../security/auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
<<<<<<< HEAD
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const currentUser = localStorage.getItem(TOKEN_NAME);
        console.log('interceptado: ', currentUser);
        if (currentUser) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser}`
                }
            });
        }
=======
>>>>>>> abf7807524d73b180c7ad9fed77d84d121fc9017

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('interceptado com sucesso');

    // add authorization header with jwt token if available
    const currentUser = JSON.parse(localStorage.getItem('jwt_token'));
    console.log('interceptado: ', currentUser);
    if (currentUser && currentUser.token) {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${currentUser.token}`
            }
        });
    }

    return next.handle(request);
  }
}
