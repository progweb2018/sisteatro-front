import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TOKEN_NAME } from '../security/auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(
      private router: Router
      ) { }

    // Permite usuário acessar rota somente caso possua token na localStorage
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem(TOKEN_NAME)) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
