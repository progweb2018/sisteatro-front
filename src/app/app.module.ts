import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpHandler, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routes';
import { NavbarComponent } from './_layout/navbar/navbar.component';
import { PagPrincipalComponent } from './_layout/pag-principal/pag-principal.component';
import { PagNaoEncontradaComponent } from './_layout/pag-nao-encontrada/pag-nao-encontrada.component';
import { AuthService } from './security/auth.service';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
// import { ToastModule } from 'ng2-toastr';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PagPrincipalComponent,
    PagNaoEncontradaComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    HttpClientModule,
    AppRoutes,

    // BrowserAnimationsModule,
    // ToastModule.forRoot()
  ],
  providers: [
    AuthService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    // { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
