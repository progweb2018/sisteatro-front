import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { SalaListaComponent } from './sala-lista/sala-lista.component';
import { SalaFormComponent } from './sala-formulario/sala-formulario.component';

const salaRoutes: Routes = [
    { path: '', component: SalaListaComponent},
    { path: 'visualizar/:id', component: SalaFormComponent},
    { path: 'novo', component: SalaFormComponent},
    { path: 'alterar/:id', component: SalaFormComponent},
];


@NgModule({
    imports: [RouterModule.forChild(salaRoutes)],
    exports: [RouterModule]
  })

  export class SalaRouting {}
