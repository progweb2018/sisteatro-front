export class Sala {
  id: number;
  nome: string;
  totalCadeiras: number;
  acessoPNE: boolean;
  camarote: boolean;
  totalCadeirasCamarote: number;
  descricao: string;
}
