import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { Sala } from './sala';
import { URL_API } from 'src/app/app.config';

@Injectable()
export class SalaService {

    constructor(private http: HttpClient) { }

    findAll(): Observable<Sala[]> {
      // const currentUser = JSON.parse(localStorage.getItem('jwt_token'));

      // const httpOptions = {
      //   headers: new HttpHeaders({
      //     'Authorization': `Bearer ${currentUser.token}`
      //   })
      // };
        return this.http
            .get<Sala[]>(`${URL_API}/sala`);
    }

    findById(id: number): Observable<Sala> {
        return this.http
            .get<Sala>(`${URL_API}/sala/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(sala: Sala): Observable<Sala> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (sala.id) {
            return this.http
                .put<Sala>(
                    `${URL_API}/sala`,
                    JSON.stringify(sala),
                    httpOptions
                );
        } else {
            return this.http
                .post<Sala>(`${URL_API}/sala`, JSON.stringify(sala), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${URL_API}/sala/${id}`);
    }

}
