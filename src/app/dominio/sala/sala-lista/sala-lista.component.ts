import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Sala } from '../sala';
import { SalaService } from '../sala.service';

@Component({
  selector: 'app-sala-lista',
  templateUrl: './sala-lista.component.html',
  styleUrls: ['./sala-lista.component.css']
})
export class SalaListaComponent implements OnInit {

    salas: Sala[];

    constructor(
      private salaService: SalaService,
      private router: Router
    ) {}

    ngOnInit() {

      this.salaService.findAll()
      .subscribe(resposta => {
        console.log(resposta);
        this.salas = resposta;
      });

    }

    excluir(produtoId: number) {
      this.salaService.excluir(produtoId)
      .subscribe(resposta => {
        alert('Sala excluída com sucesso');
        window.location.reload();
        this.router.navigate(['/sala']);
      } );
    }

}
