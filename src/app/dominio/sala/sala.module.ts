import { JwtInterceptor } from './../../_helpers/jwt.interceptor';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { SalaService } from './sala.service';   
import { SalaListaComponent } from './sala-lista/sala-lista.component';
import { SalaFormComponent } from './sala-formulario/sala-formulario.component';
import { SalaRouting } from './sala.routing';

@NgModule({
    declarations: [
        SalaListaComponent,
        SalaFormComponent
    ],
    imports: [
        // Angular
        HttpClientModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        SalaRouting
    ],
    providers: [
        // Serviços
        SalaService,
    ]
})

export class SalaModule { }
