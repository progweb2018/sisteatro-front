import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Sala } from '../sala';
import { SalaService } from '../sala.service';

@Component({
  selector: 'app-sala-lista',
  templateUrl: './sala-formulario.component.html',
  styleUrls: ['./sala-formulario.component.css']
})
export class SalaFormComponent implements OnInit {

  sala: Sala;

  salaForm: FormGroup;
  titulo: string;
  camarote: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private salaService: SalaService
  ) { }

  ngOnInit() {

    this.sala = new Sala();

    /* Obter o `ID` passado por parâmetro na URL */
    this.sala.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.sala.id == null)
    ? 'Nova Sala'
    : 'Alterar Sala';

    /* Reactive Forms */
    this.salaForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      descricao: this.builder.control('', [Validators.required]),
      totalCadeiras: this.builder.control('', [Validators.required, Validators.maxLength(200)]),
      acessoPNE: this.builder.control('', []),
      camarote: this.builder.control('', []),
      totalCadeirasCamarote: this.builder.control('', []),
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.sala.id != null) {
      this.salaService.findById(this.sala.id)
        .subscribe(retorno => {
          // Atualiza o formulário com os valores retornados
          this.salaForm.patchValue(retorno);

        });
    }
  }

  salvar(sala: Sala) {
    if (this.salaForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.salaService.salvar(sala)
      .subscribe(response => {
        console.log('Curso salvo com sucesso');

        // retorna para a lista
        this.router.navigate(['/sala']);
      },
      (error) => {
        console.log('Erro no back-end');
      });
    }
  }

}
