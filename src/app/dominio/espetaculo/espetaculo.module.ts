import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { EspetaculoService } from './espetaculo.service';
import { EspetaculoListaComponent } from './espetaculo-lista/espetaculo-lista.component';
import { EspetaculoFormularioComponent } from './espetaculo-formulario/espetaculo-formulario.component';
import { EspetaculoRouting } from './espetaculo.routing';

@NgModule({
    declarations: [
        EspetaculoListaComponent,
        EspetaculoFormularioComponent
    ],
    imports: [
        // Angular
        HttpClientModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        EspetaculoRouting
    ],
    providers: [
        // Serviços
        EspetaculoService
    ]
})

export class EspetaculoModule { }
