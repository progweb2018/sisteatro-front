export class Espetaculo {
  id: number;
  nome: string;
  classificacao: number;
  genero: string;
  duracao: number;
  data: boolean;
  descricao: string;
}
