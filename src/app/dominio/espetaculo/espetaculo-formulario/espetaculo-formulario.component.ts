import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Espetaculo } from '../espetaculo';
import { EspetaculoService } from '../espetaculo.service';

@Component({
  selector: 'app-espetaculo-form',
  templateUrl: './espetaculo-formulario.component.html',
  styleUrls: ['./espetaculo-formulario.component.css']
})
export class EspetaculoFormularioComponent implements OnInit {

  espetaculo: Espetaculo;

  espetaculoForm: FormGroup;
  titulo: string;
  camarote: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private espetaculoService: EspetaculoService
  ) { }

  ngOnInit() {

    this.espetaculo = new Espetaculo();

    /* Obter o `ID` passado por parâmetro na URL */
    this.espetaculo.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.espetaculo.id == null)
    ? 'Nova Espetaculo'
    : 'Alterar Espetaculo';

    /* Reactive Forms */
    this.espetaculoForm = this.builder.group({
      id: [],
      nome: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      genero: this.builder.control('', [Validators.required, Validators.maxLength(50)]),
      data: this.builder.control(''),
      sinopse: this.builder.control('', [Validators.maxLength(80)]),
      classificacao: this.builder.control('', [Validators.maxLength(20)]),
      duracao: this.builder.control(''),
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.espetaculo.id != null) {
      this.espetaculoService.findById(this.espetaculo.id)
        .subscribe(retorno => {
          // Atualiza o formulário com os valores retornados
          this.espetaculoForm.patchValue(retorno);

        });
    }
  }

  salvar(espetaculo: Espetaculo) {
    if (this.espetaculoForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.espetaculoService.salvar(espetaculo)
      .subscribe(response => {
        console.log('Espetaculo salvo com sucesso');

        // retorna para a lista
        this.router.navigate(['/espetaculo']);
      },
      (error) => {
        console.log('Erro no back-end');
      });
    }
  }

}
