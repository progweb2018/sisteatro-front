import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';


import { Espetaculo } from './espetaculo';
import { URL_API } from 'src/app/app.config';

@Injectable()
export class EspetaculoService {

    constructor(private http: HttpClient) { }

    findAll(): Observable<Espetaculo[]> {
        return this.http
            .get<Espetaculo[]>(`${URL_API}/espetaculo`);
    }

    findById(id: number): Observable<Espetaculo> {
        return this.http
            .get<Espetaculo>(`${URL_API}/espetaculo/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(espetaculo: Espetaculo): Observable<Espetaculo> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (espetaculo.id) {
            return this.http
                .put<Espetaculo>(
                    `${URL_API}/espetaculo`,
                    JSON.stringify(espetaculo),
                    httpOptions
                );
        } else {
            return this.http
                .post<Espetaculo>(`${URL_API}/espetaculo`, JSON.stringify(espetaculo), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${URL_API}/espetaculo/${id}`);
    }

}
