import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Espetaculo } from '../espetaculo';
import { EspetaculoService } from '../espetaculo.service';

@Component({
  selector: 'app-espetaculo-lista',
  templateUrl: './espetaculo-lista.component.html',
  styleUrls: ['./espetaculo-lista.component.css']
})
export class EspetaculoListaComponent implements OnInit {

    espetaculos: Espetaculo[];

    constructor(
      private espetaculoService: EspetaculoService,
      private router: Router
    ) {}

    ngOnInit() {

      this.espetaculoService.findAll()
      .subscribe(resposta => {
        this.espetaculos = resposta;
      });

    }

    excluir(produtoId: number) {
      this.espetaculoService.excluir(produtoId)
      .subscribe(resposta => {
        alert('Espetaculo excluído com sucesso');
        window.location.reload();
        this.router.navigate(['/espetaculo']);
      } );
    }

}
