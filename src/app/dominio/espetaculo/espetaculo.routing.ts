import { EspetaculoFormularioComponent } from './espetaculo-formulario/espetaculo-formulario.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { EspetaculoListaComponent } from './espetaculo-lista/espetaculo-lista.component';

const espetaculoRoutes: Routes = [
    { path: '', component: EspetaculoListaComponent},
    { path: 'visualizar/:id', component: EspetaculoFormularioComponent},
    { path: 'novo', component: EspetaculoFormularioComponent},
    { path: 'alterar/:id', component: EspetaculoFormularioComponent},
];


@NgModule({
    imports: [RouterModule.forChild(espetaculoRoutes)],
    exports: [RouterModule]
  })

  export class EspetaculoRouting {}
