import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';

import { Sessao } from './sessao';
import { URL_API } from 'src/app/app.config';

@Injectable()
export class SessaoService {

    constructor(private http: HttpClient) { }

    findAll(): Observable<Sessao[]> {
        return this.http
            .get<Sessao[]>(`${URL_API}/sessao`);
    }

    findById(id: number): Observable<Sessao> {
        return this.http
            .get<Sessao>(`${URL_API}/sessao/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(sessao: Sessao): Observable<Sessao> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (sessao.id) {
            return this.http
                .put<Sessao>(
                    `${URL_API}/sessao`,
                    JSON.stringify(sessao),
                    httpOptions
                );
        } else {
            return this.http
                .post<Sessao>(`${URL_API}/sessao`, JSON.stringify(sessao), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${URL_API}/sessao/${id}`);
    }

}
