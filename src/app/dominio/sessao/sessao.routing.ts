import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { SessaoListaComponent } from 'src/app/dominio/sessao/sessao-lista/sessao-lista.component';
import { SessaoFormComponent } from 'src/app/dominio/sessao/sessao-formulario/sessao-formulario.component';

const sessaoRoutes: Routes = [
    { path: '', component: SessaoListaComponent},
    { path: 'visualizar/:id', component: SessaoFormComponent},
    { path: 'novo', component: SessaoFormComponent},
    { path: 'alterar/:id', component: SessaoFormComponent},
];

@NgModule({
    imports: [RouterModule.forChild(sessaoRoutes)],
    exports: [RouterModule]
  })

  export class SessaoRouting {}
