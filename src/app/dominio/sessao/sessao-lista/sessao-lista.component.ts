import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Sessao } from '../sessao';
import { SessaoService } from '../sessao.service';

@Component({
  selector: 'app-sessao-lista',
  templateUrl: './sessao-lista.component.html',
  styleUrls: ['./sessao-lista.component.css']
})
export class SessaoListaComponent implements OnInit {

    sessoes: Sessao[];

    constructor(
      private sessaoService: SessaoService,
      private router: Router
    ) {}

    ngOnInit() {

      this.sessaoService.findAll()
      .subscribe(resposta => {
        this.sessoes = resposta;
        console.log(resposta);
      });

    }

    excluir(produtoId: number) {
      this.sessaoService.excluir(produtoId)
      .subscribe(resposta => {
        alert('Sessao excluída com sucesso');
        this.router.navigate(['/sessao']);
        window.location.reload();
      } );
    }

}
