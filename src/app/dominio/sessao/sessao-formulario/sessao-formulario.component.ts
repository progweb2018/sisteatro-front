import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Sessao } from '../sessao';
import { SessaoService } from '../sessao.service';
import { Sala } from '../../sala/sala';
import { SalaService } from '../../sala/sala.service';

@Component({
  selector: 'app-sessao-lista',
  templateUrl: './sessao-formulario.component.html',
  styleUrls: ['./sessao-formulario.component.css']
})
export class SessaoFormComponent implements OnInit {

  sessao: Sessao;
  horarios: FormArray;

  sessaoForm: FormGroup;
  titulo: string;

  salas: Sala[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private sessaoService: SessaoService,
    private salaService: SalaService
  ) { }

  ngOnInit() {

    this.salaService.findAll()
    .subscribe(retorno => {
      this.salas = retorno;
      console.log('salas em sessao: ', retorno);
    });

    this.sessao = new Sessao();

    /* Obter o `ID` passado por parâmetro na URL */
    this.sessao.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.sessao.id == null) ? 'Nova Sessao' : 'Alterar Sessao';

    /* Reactive Forms */
    this.sessaoForm = this.builder.group({
      id: [],
      data: this.builder.control('', [Validators.required]),
      horarios: this.builder.array([ this.createItem() ]),
      preco: this.builder.control('', [Validators.required, Validators.maxLength(5)]),
      sala: this.builder.control('', [Validators.required]),
    }, {});

    // Se existir `ID` realiza busca para trazer os dados
    if (this.sessao.id != null) {
      this.sessaoService.findById(this.sessao.id)
        .subscribe(retorno => {
          // Atualiza o formulário com os valores retornados
          this.sessaoForm.patchValue(retorno);
        });
    }

  }

  createItem(): FormGroup {
    return this.builder.group({
      horario: '',
    });
  }

  addItem(): void {
    this.horarios = this.sessaoForm.get('horarios') as FormArray;
    this.horarios.push(this.createItem());
  }

  removeItem(id: number) {
    this.horarios.removeAt(id);

  }

  salvar(sessao: Sessao) {
    if (this.sessaoForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.sessaoService.salvar(sessao)
      .subscribe(response => {
        console.log('Curso salvo com sucesso');

        // retorna para a lista
        this.router.navigate(['/sessao']);
      },
      (error) => {
        console.log('Erro no back-end');
      });
    }
  }

}
