import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { SessaoRouting } from 'src/app/dominio/sessao/sessao.routing';
import { SessaoService } from 'src/app/dominio/sessao/sessao.service';
import { SessaoListaComponent } from 'src/app/dominio/sessao/sessao-lista/sessao-lista.component';
import { SessaoFormComponent } from 'src/app/dominio/sessao/sessao-formulario/sessao-formulario.component';
import { SalaService } from '../sala/sala.service';

@NgModule({
    declarations: [
        SessaoListaComponent,
        SessaoFormComponent
    ],
    imports: [
        // Angular
        HttpClientModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        SessaoRouting
    ],
    providers: [
        // Serviços
        SessaoService,
        SalaService
    ]
})

export class SessaoModule { }
