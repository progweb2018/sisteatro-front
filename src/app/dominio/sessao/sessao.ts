import { Sala } from '../sala/sala';

export class Sessao {
  // substituir por entidade de espetaculo
  // espetaculo: string;
  id: number;
  data: Date;
  horario: Date;
  preco: number;
  sala: Sala;
}
