import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// import { VendaListaComponent } from './venda-lista/venda-lista.component';
import { VendaFormComponent } from './venda-form/venda-form.component';

const vendaRoutes: Routes = [
    // { path: '', component: VendaListaComponent},
    { path: 'visualizar/:id', component: VendaFormComponent},
    { path: 'novo', component: VendaFormComponent},
    { path: 'alterar/:id', component: VendaFormComponent},
];


@NgModule({
    imports: [RouterModule.forChild(vendaRoutes)],
    exports: [RouterModule]
  })

  export class VendaRouting {}
