import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Venda } from '../venda';
import { VendaService } from '../venda.service';



@Component({
  selector: 'app-venda-lista',
  templateUrl: './venda-form.component.html',
  styleUrls: ['./venda-form.component.css']
})
export class VendaFormComponent implements OnInit {

  venda: Venda;

  vendaForm: FormGroup;
  titulo: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private builder: FormBuilder,
    private vendaService: VendaService
  ) { }

  ngOnInit() {

    this.venda = new Venda();

    /* Obter o `ID` passado por parâmetro na URL */
    this.venda.id = this.route.snapshot.params['id'];

    /* Altera o título da página */
    this.titulo = (this.venda.id == null)
    ? 'Novo Venda'
    : 'Alterar Venda';

    /* Reactive Forms */
    this.vendaForm = this.builder.group({
      id: Number,
      tipoDeEntrada: Text,
      selecionarAssento: Text,
      acessoPNE: Boolean,
      selecionarSessao: Text,
      horarioSessao: Text,
      valorIngresso: Number,
    }, {});

  }

  salvar(venda: Venda) {
    if (this.vendaForm.invalid) {
      console.log('Erro no formulário');
    } else {
      this.vendaService.salvar(venda)
      .subscribe(response => {
        console.log('Curso salvo com sucesso');

        // retorna para a lista
        this.router.navigate(['/venda']);
      },
      (error) => {
        console.log('Erro no back-end');
      });
    }
  }

}
