import { JwtInterceptor } from './../../_helpers/jwt.interceptor';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { VendaService } from './venda.service';   
// import { VendaListaComponent } from './venda-lista/venda-lista.component';
import { VendaFormComponent } from './venda-form/venda-form.component';
import { VendaRouting } from './venda.routing';

@NgModule({
    declarations: [
        // VendaListaComponent,
        VendaFormComponent
    ],
    imports: [
        // Angular
        HttpClientModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        VendaRouting
    ],
    providers: [
        // Serviços
        VendaService,
    ]
})

export class VendaModule { }
