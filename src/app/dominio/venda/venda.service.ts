import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';

import { Venda } from './venda';
import { URL_API } from 'src/app/app.config';

@Injectable()
export class VendaService {

    constructor(private http: HttpClient) { }

    findAll(): Observable<Venda[]> {
        return this.http
            .get<Venda[]>(`${URL_API}/venda`);
    }

    findById(id: number): Observable<Venda> {
        return this.http
            .get<Venda>(`${URL_API}/venda/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(venda: Venda): Observable<Venda> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (venda.id) {
            return this.http
                .put<Venda>(
                    `${URL_API}/venda`,
                    JSON.stringify(venda),
                    httpOptions
                );
        } else {
            return this.http
                .post<Venda>(`${URL_API}/venda`, JSON.stringify(venda), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${URL_API}/venda/${id}`);
    }

}
