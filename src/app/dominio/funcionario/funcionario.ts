export class Funcionario {
  id: number;
  nome: string;
  cpf: number;
  pais: string;
  bairro: string;
  cargo: string;
  usuario: string;
  senha: string;
  rg: number;
  dataDeNascimento: Date;
  email: string;
  telefone: number;
}

