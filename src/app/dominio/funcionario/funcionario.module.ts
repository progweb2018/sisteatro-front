import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { FuncionarioService } from './funcionario.service';
import { FuncionarioListaComponent } from './funcionario-lista/funcionario-lista.component';
import { FuncionarioFormComponent } from './funcionario-form/funcionario-form.component';
import { FuncionarioRouting } from './funcionario.routing';

@NgModule({
    declarations: [
        FuncionarioListaComponent,
        FuncionarioFormComponent
    ],
    imports: [
        // Angular
        HttpClientModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        FuncionarioRouting
    ],
    providers: [
        // Serviços
        FuncionarioService
    ]
})

export class FuncionarioModule { }
