import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';

import { Funcionario } from './funcionario';
import { URL_API } from 'src/app/app.config';

@Injectable()
export class FuncionarioService {

    constructor(private http: HttpClient) { }

    findAll(): Observable<Funcionario[]> {
        return this.http
            .get<Funcionario[]>(`${URL_API}/funcionario`);
    }

    findById(id: number): Observable<Funcionario> {
        return this.http
            .get<Funcionario>(`${URL_API}/funcionario/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(funcionario: Funcionario): Observable<Funcionario> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (funcionario.id) {
            return this.http
                .put<Funcionario>(
                    `${URL_API}/funcionario`,
                    JSON.stringify(funcionario),
                    httpOptions
                );
        } else {
            return this.http
                .post<Funcionario>(`${URL_API}/funcionario`, JSON.stringify(funcionario), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${URL_API}/funcionario/${id}`);
    }

}
