import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { FuncionarioListaComponent } from './funcionario-lista/funcionario-lista.component';
import { FuncionarioFormComponent } from './funcionario-form/funcionario-form.component';

const funcionarioRoutes: Routes = [
    { path: '', component: FuncionarioListaComponent},
    { path: 'visualizar/:id', component: FuncionarioFormComponent},
    { path: 'novo', component: FuncionarioFormComponent},
    { path: 'alterar/:id', component: FuncionarioFormComponent},
];


@NgModule({
    imports: [RouterModule.forChild(funcionarioRoutes)],
    exports: [RouterModule]
  })

  export class FuncionarioRouting {}
