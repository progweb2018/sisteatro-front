import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Funcionario } from '../funcionario';
import { FuncionarioService } from '../funcionario.service';

@Component({
  selector: 'app-funcionario-lista',
  templateUrl: './funcionario-lista.component.html',
  styleUrls: ['./funcionario-lista.component.css']
})
export class FuncionarioListaComponent implements OnInit {

    funcionarios: Funcionario[];

    constructor(
      private funcionarioService: FuncionarioService,
      private router: Router
    ) {}

    ngOnInit() {

      this.funcionarioService.findAll()
      .subscribe(resposta => {
        this.funcionarios = resposta;
      });

    }

    excluir(produtoId: number) {
      this.funcionarioService.excluir(produtoId)
      .subscribe(resposta => {
        alert('Funcionario excluído com sucesso');
        window.location.reload();
        this.router.navigate(['/funcionario']);
      } );
    }

}
