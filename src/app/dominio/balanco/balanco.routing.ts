import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// import { BalancoListaComponent } from './balanco-lista/balanco-lista.component';
import { BalancoFormComponent } from './balanco-form/balanco-form.component';

const balancoRoutes: Routes = [
    // { path: '', component: BalancoListaComponent},
    { path: 'visualizar/:id', component: BalancoFormComponent},
    { path: 'novo', component: BalancoFormComponent},
    { path: 'alterar/:id', component: BalancoFormComponent},
];


@NgModule({
    imports: [RouterModule.forChild(balancoRoutes)],
    exports: [RouterModule]
  })

  export class BalancoRouting {}
