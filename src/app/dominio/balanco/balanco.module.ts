import { JwtInterceptor } from './../../_helpers/jwt.interceptor';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { BalancoService } from './balanco.service';   
// import { BalancoListaComponent } from './balanco-lista/balanco-lista.component';
import { BalancoFormComponent } from './balanco-form/balanco-form.component';
import { BalancoRouting } from './balanco.routing';

@NgModule({
    declarations: [
        // BalancoListaComponent,
        BalancoFormComponent
    ],
    imports: [
        // Angular
        HttpClientModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

        // Componente
        BalancoRouting
    ],
    providers: [
        // Serviços
        BalancoService,
    ]
})

export class BalancoModule { }
