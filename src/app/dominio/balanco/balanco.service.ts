import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError, last, map, tap } from 'rxjs/operators';

import { Balanco } from './balanco';
import { URL_API } from 'src/app/app.config';

@Injectable()
export class BalancoService {

    constructor(private http: HttpClient) { }

    findAll(): Observable<Balanco[]> {
        return this.http
            .get<Balanco[]>(`${URL_API}/balanco`);
    }

    findById(id: number): Observable<Balanco> {
        return this.http
            .get<Balanco>(`${URL_API}/balanco/${id}`)
            .pipe(
                map(response => response)
            );
    }

    salvar(balanco: Balanco): Observable<Balanco> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };

        if (balanco.id) {
            return this.http
                .put<Balanco>(
                    `${URL_API}/balanco`,
                    JSON.stringify(balanco),
                    httpOptions
                );
        } else {
            return this.http
                .post<Balanco>(`${URL_API}/balanco`, JSON.stringify(balanco), httpOptions);
        }
    }

    excluir(id: number): Observable<any> {
        return this.http
            .delete(`${URL_API}/balanco/${id}`);
    }

}
